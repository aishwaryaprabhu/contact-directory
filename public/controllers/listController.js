app.controller('listCtrl', ['$scope', '$http', '$routeParams', 'Upload', '$filter', 'MyService', 'MyFactory', function($scope, $http, $routeParams, Upload, $filter, MyService, MyFactory) {
    
	console.log("list ...");

	$scope.fileUpload = function(){
		console.log($scope.file);

		Upload.upload({
			url: "home/upload",
			data : {file: $scope.file}
		}).then(function (resp) {
            console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });
	}


 	$scope.fetchItem = function(){
 		var id = $routeParams._id;
 		$http.post("api/getItem", {id: id})
 		.success(function(data, webStatus, headers, config){
 			console.log(data);
 			$scope.item = data;
 		}).error(function(data, webStatus, headers, config){
 			console.log(data);
 		});
 	}

 	$scope.fetchList = function(){
 		 $http.get("/api/list")
	    .then(function(response) {
	       console.log(response);
	       $scope.list = response.data;
	    });
 	}

 	// $scope.fetchList = function(){
 	// 	 $http.post("/api/list", {var : 1})
 	// 	 .success(function(data, webStatus, headers, config){
		// 	console.log(data);
		// 	$scope.list = data;
	 //       console.log($scope.list);
 	// 	}).error(function(data, webStatus, headers, config){
 	// 		console.log(data);
 	// 	});
 	// }

 	$scope.addItem = function(item){
 		$http.post("api/add", item)
 		.success(function(data, webStatus, headers, config){
			console.log(data);
 		}).error(function(data, webStatus, headers, config){
 			console.log(data);
 		});
 	}

 	$scope.deleteItem = function(itemId){ 
 		$http.post("api/deleteItem", {id : itemId})
 		.success(function(data, webStatus, headers, config){
 			console.log(data);
 			$scope.fetchList();
 		}).error(function(data, webStatus, headers, config){
 			console.log(data);
 		});
 	}

 	$scope.editItem = function(item){
 		$http.post("api/update", item)
 		.success(function(data, webStatus, headers, config){
 			console.log(data);
 			$scope.item = data;
 		}).error(function(webStatus, headers, config){
 			console.log("Server Error");
 		});
 	}
   
 	$scope.fetchList();

 	$scope.msg = MyService.test;
 	console.log($scope.msg);
 	console.log(MyService.date);
 	MyService.testfunction();

 	$scope.call = "call from listCtrl";
 	MyFactory($scope.call);
}]);
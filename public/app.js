var app = angular.module('Project', ['ngRoute', 'ngFileUpload']);

var base_url = "http://localhost:2000/";

app.service('MyService', function(){
	this.test = "MyService";
	this.date = new Date();
	this.testfunction = function(){
		var testdata = "called by LIst controller";
		console.log(testdata);
	}
});

app.factory('MyFactory',function(){
	return function test(msg){
		var testdata = {};
		testdata.test = "MyFactory";
		testdata.msg = msg;
		console.log(testdata);
	};
});


app.directive('myDirective',  function(){
	// Runs during compile
	return {
		// name: '',
		// priority: 1,
		// terminal: true,
		// scope: {}, // {} = isolate, true = child, false/undefined = no change
		// controller: function($scope, $element, $attrs, $transclude) {},
		// require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
		// restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
		// template: '',
		// templateUrl: '',
		// replace: true,
		// transclude: true,
		// compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
		// link: function($scope, iElm, iAttrs, controller) {
			
		// }

		restrict: 'E',
		scope: {
			data : '='
		},
		controller: function($scope){
			console.log('myDirective');
			console.log($scope.data);
		}
	};
});

app.config(function($routeProvider) {
	$routeProvider
	.when('/', {
		templateUrl : 'views/mylist.html',
		controller : 'listCtrl'
	})
	.when('/aboutus', {
		templateUrl : 'views/about.html',
		controller : 'aboutCtrl'
	})
	.when('/feedback', {
		templateUrl : 'views/feedback.html',
		controller : 'aboutCtrl'
	})
	.when('/edit/:_id', {
		templateUrl : 'views/editlist.html',
		controller : 'listCtrl'
	})
	.when('/add', {
		controller : 'listCtrl',
		templateUrl : 'views/addlist.html'
	});
	
});

const mongoose = require("../connection.js");

const userSchema = new mongoose.Schema({
	fname: String,
	lname: String,
	email: String
});

const User = mongoose.model('user_details', userSchema);

module.exports = User;





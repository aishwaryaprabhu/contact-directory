const mongoose = require("mongoose");

mongoose.connect("mongodb://localhost/mylist");

mongoose.connection.once('open', () => {
    console.log('connection establised');
}).on('error', (error)  => {
    console.log("Connection error : " + error);
});

module.exports = mongoose;
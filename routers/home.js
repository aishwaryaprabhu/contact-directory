var express = require('express');
var router = express.Router();

router.get('/', function(req, res){
	res.send("Index page");
	// res.render('index.html');
});

router.post('/upload', function(req, res){
	console.log("upload router");

	if (!req.files)
    return res.status(400).send('No files were uploaded.');

	var  myFile = req.files.file;
	console.log(myFile);
	//console.log('../public/uploadfiles/' + myFile.name);
	myFile.mv('public/uploadfiles/'+ myFile.name, function(err) {
		if (err)
		  return res.status(500).send(err);
	 
	    res.send('File uploaded!');
	});
});

module.exports = router;
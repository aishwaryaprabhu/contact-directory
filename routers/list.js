var express = require('express');
var router = express.Router();

const User = require('../models/user_details.js');

router.get('/list', function(req, res){
	User.find({},function(err, doc){
		if(err){
			res.status(401).send(err);	
		}
		res.send(doc);

	});
});


router.post('/add', function(req, res){
	var item = {
		fname : req.body.fname,
		lname : req.body.lname,
		email : req.body.email
	};
	
	var data = new User(item);
	data.save(function(err, user){
		if(err){
			console.log(err);
		}
		res.status(200).send("success!!");
	});
});

router.post('/update', function(req, res){
	var _id = req.body._id;
	console.log(req.body);
	User.findByIdAndUpdate(_id,
		{fname: req.body.fname, lname: req.body.lname, email: req.body.email},{new:true}, 
		function(err, doc){
			
		if(err){
			res.status(401).send(err);
		}
		res.status(200).send(doc);
	})
});

// router.post('/update', function(req, res){
// 	var _id = req.body._id;
// 	console.log(req.body);
// 	User.findOneAndUpdate({fname:'selwyn'}, 
// 		{fname:'ash'},
// 		function(err, doc){
// 		if(err){
// 			res.status(401).send(err);
// 		}
// 		res.status(200).send(doc);
// 		console.log(doc);
// 	})
// });


router.post('/deleteItem', function(req, res){
	var _id = req.body.id;
	console.log(_id);

	User.findByIdAndRemove(_id, function(err) {
	  if(err){
			res.status(401).send(err);
		}
	  console.log('User deleted!');
	  res.status(200).send("Item Deleted");
	});

	// User.findById(_id, function(err, doc){
	// 	if(err){
	// 		//res.status(401).send(err);
	// 		console.log(err);
	// 	}
	// 	console.log(doc);
	// 	doc.remove(function(err){
	// 		//res.status(401).send(err);
	// 	});
	// 	//res.status(200).send("Item Deleted");
	// });

});

router.post('/getItem', function(req, res){
	var _id = req.body.id;
	User.findById(_id, function(err, doc){
		if(err){
			res.status(401).send(err);
		}
		console.log(doc);
		res.status(200).send(doc);
	});
});

module.exports = router;
var express = require('express');
var http = require('http');
var bodyParser = require('body-parser');
var fileUpload = require('express-fileupload');

var app = express();
app.use(fileUpload());

// app.get('/', function(req, res){
// 	res.send("Hello World");
// });

app.use(bodyParser.json());

app.use("/", express.static(__dirname + "/public"));

app.use("/home", require(__dirname + "/routers/home"));
app.use("/api", require(__dirname + "/routers/list"));

app.listen(2000, function(){
	console.log("Server started on port 3000");
});